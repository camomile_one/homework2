public class SpamAnalyzer extends AbstractKeywordAnalyzer {
    public SpamAnalyzer(String[] keywords) {
        this.keywords = new String[keywords.length];
        System.arraycopy(keywords, 0, this.keywords, 0, keywords.length);
    }

    @Override
    protected Label getLabel() {
        return Label.SPAM;
    }
}
