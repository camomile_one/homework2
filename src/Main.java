public class Main {
    public static Label check(TextAnalyzer[] analyzers, String text) {
        Label L = Label.OK;
        for (int i = 0; i < 3; i++) {
            L = analyzers[i].processText(text);
            if (Label.OK != L) break;
        }
        return L;
    }

    public static void main (String[] args) {
        TextAnalyzer a = new SpamAnalyzer(new String[] {"super", "xxx"});
        TextAnalyzer b = new NegativeTextAnalyzer();
        TextAnalyzer c = new TooLongTextAnalyzer(140);
        System.out.println(check(new TextAnalyzer[]{a, b, c}, "Test text"));
    }
}
