public class NegativeTextAnalyzer extends AbstractKeywordAnalyzer {
    public NegativeTextAnalyzer() {
        keywords = new String[]{":(", "=(", ":|"};
    }

    @Override
    protected Label getLabel() {
        return Label.NEGATIVE_TEXT;
    }
}
